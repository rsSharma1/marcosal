#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<sys/sem.h>

struct sembuf semop1;

union semun {
	int val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
	struct seminfo  *__buf;  /* Buffer for IPC_INFO(Linux-specific) */
}sem;

void set_critical(int sem_id)
{
	semop1.sem_num=0;
	semop1.sem_op=-1;
	semop1.sem_flg=SEM_UNDO;
	semop(sem_id,&semop1,1);
	// semctl(sem_id,0,GETVAL,1);
}

void exit_critical(int sem_id)
{
	semop1.sem_num=0;
	semop1.sem_op=1;
	semop1.sem_flg=SEM_UNDO;
	semop(sem_id,&semop1,1);
	// semctl(sem_id,0,GETVAL,1);
}

struct data{
	int a,b;
	int tag;
	char oper;
};

struct result{
	int res;
	int tag;
};




int main(int argc,char *argv[])
{
	long int x;
	int fd,ret,msg_id,key;
	struct data data1;
	struct result ptr;

	key=semget(111,1,0666);
	//printf("key in client1=%d\n",key);
	msg_id=msgget((key_t)101,0666|IPC_CREAT);
	if(msg_id==-1)
	{
		printf("msgqueue not created\n");
	}
	//printf("msg_id=%d\n",(int)msg_id);
	set_critical(key);

	data1.a=10;
	data1.b=20;
	data1.oper='+';
	data1.tag=111;
	
	fd=open("myfifo",O_WRONLY);

	ret=write(fd,&data1,16);

	printf("wrote %d no. of bytes in client1\n",ret);
	printf("pid of client1=%d\n",getpid());
	x=ptr.tag;

	msgrcv(msg_id,(void*)&ptr,8,x,0);
	printf("RESULT IN requesting client1 and tag = %d and %d\n",ptr.res,ptr.tag);
	printf("------------------------------------END OF CLIENT1--------------------------------------\n");i

	exit_critical(key);
	//msgctl(msg_id,IPC_RMID,NULL);
	return 0;
}
