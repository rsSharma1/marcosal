#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<sys/sem.h>

struct sembuf semop1;
union semun {
	int val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
	struct seminfo  *__buf;  /* Buffer for IPC_INFO(Linux-specific) */
}sem;

void set_critical(int sem_id)
{
	semop1.sem_num=0;
	semop1.sem_op=-1;
	semop1.sem_flg=SEM_UNDO;
	semop(sem_id,&semop1,1);
}
void exit_critical(int sem_id)
{
	semop1.sem_num=0;
	semop1.sem_op=1;
	semop1.sem_flg=SEM_UNDO;
	semop(sem_id,&semop1,1);
}

struct data{
	int a,b;
	int tag;
	char oper;
};

struct result{
	int res;
	int tag;
};

int main(int argc,char *argv[])
{
	long int x;
	int fd,ret,msg_id,key;
	struct data data1;
	struct result ptr;

	key=semget(111,1,0666);

	msg_id=msgget((key_t)101,0666|IPC_CREAT);
	if(msg_id==-1)
	{
		printf("msgqueue not created\n");
	}

	set_critical(key);
	printf("Getpid r_client2=%d\n",getpid());

	data1.a=20;
	data1.b=5;
	data1.oper='-';
	data1.tag=121;
	printf("SENDING a=%d and b=%d with operator = subtract\n",data1.a,data1.b);

	fd=open("myfifo",O_WRONLY);

	ret=write(fd,&data1,16);
	printf("Wrote %d no. of bytes in client2\n",ret);

	x=ptr.tag;
	msgrcv(msg_id,(void*)&ptr,8,x,0);

	printf("RESULT IS = %d\n",ptr.res);
	printf("-------------------------------------END OF CLIENT2------------------------------------\n");
	exit_critical(key);

	//msgctl(msg_id,IPC_RMID,NULL);
	return 0;
}
