#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<sys/sem.h>
#include<pthread.h>
void *thread1(void *);
void *thread2(void *);
void *thread3(void *);
void *thread4(void *);
struct sembuf semop1;
  union semun {
                    int val;    /* Value for SETVAL */
                    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
                    unsigned short  *array;  /* Array for GETALL, SETALL */
                    struct seminfo  *__buf;  /* Buffer for IPC_INFO(Linux-specific) */
                 }sem;
  
  void set_critical(int sem_id)
           {
           semop1.sem_num=0;
           semop1.sem_op=-1;
          semop1.sem_flg=SEM_UNDO;
 semop(sem_id,&semop1,1);
//semctl(sem_id,0,GETVAL,1);
           }
   void exit_critical(int sem_id)
                   {
   semop1.sem_num=0;
   semop1.sem_op=1;
 semop1.sem_flg=SEM_UNDO;
 semop(sem_id,&semop1,1);
//semctl(sem_id,0,GETVAL,1);
}
struct data{
int a,b;
int tag;
char oper;
};
struct result{
int res;
int tag;
};
int fd,ret,key;
struct data data1,data2;
struct result ptr,ptr1,ptr2,ptr3;




int main(int argc,char *argv[])
{
int arg=10;
void * retval1;
void *retval2;
pthread_t thd_id[4];
key=semget(111,1,0666);
//printf("key of semaphore in client 6 is %d\n",key);


set_critical(key);
ret=pthread_create(&thd_id[0],NULL,thread1,&arg);
if(ret)
printf("thread not formed\n");
pthread_join(thd_id[0],(void*)&retval1);
printf("retvalue =%d\n",*((int*)retval1));


ret=pthread_create(&thd_id[1],NULL,thread2,&arg);
if(ret)
printf("thread not formed\n");
pthread_join(thd_id[1],(void*)&retval1);
printf("retvalue =%d\n",*((int*)retval1));


ret=pthread_create(&thd_id[2],NULL,thread3,&arg);
if(ret)
printf("thread not formed\n");
pthread_join(thd_id[1],(void*)&retval1);
printf("retvalue =%d\n",*((int*)retval1));



ret=pthread_create(&thd_id[3],NULL,thread4,&arg);
if(ret)
printf("thread not formed\n");
pthread_join(thd_id[1],(void*)&retval1);
printf("retvalue =%d\n",*((int*)retval1));

exit_critical(key);
//msgctl(msg_id,IPC_RMID,NULL);
return 0;
}



void *thread1(void *arg)
{
int arg2=1,msg_id;
long int x;
printf("arg=%d\n",*((int*)arg));
msg_id=msgget((key_t)101,0666|IPC_CREAT);
if(msg_id==-1)
{
printf("msgqueue not created\n");
}


data1.a=8;
data1.b=5;
data1.oper='+';
data1.tag=132;

fd=open("myfifo",O_WRONLY);
ret=write(fd,&data1,16);
printf("wrote %d no. of bytes in client7 and thread1\n",ret);

x=ptr.tag;
msgrcv(msg_id,(void*)&ptr,8,x,0);
printf("RESULT IN requesting client6 and thread7 and  tag = %d and %d\n",ptr.res,ptr.tag);
printf("---------------------------END OF CLIENT7  thread1----------------------------------\n");
close(fd);
pthread_exit(&arg2);
}




void *thread2(void *arg)
{
int arg2=1,msg_id;
long int x;
printf("arg=%d\n",*((int*)arg));
msg_id=msgget((key_t)101,0666|IPC_CREAT);
if(msg_id==-1)
{
printf("msgqueue not created\n");
}


data1.a=30;
data1.b=19;
data1.oper='-';
data1.tag=133;

fd=open("myfifo",O_WRONLY);
ret=write(fd,&data1,16);
printf("wrote %d no. of bytes in client7 and thread2\n",ret);

x=ptr1.tag;
msgrcv(msg_id,(void*)&ptr1,8,x,0);
printf("RESULT IN requesting client7 and thread2 and  tag = %d and %d\n",ptr1.res,ptr1.tag);
printf("---------------------------END OF CLIENT7  thread2----------------------------------\n");
close(fd);
pthread_exit(&arg2);
}




void *thread3(void *arg)
{
int arg2=1,msg_id;
long int x;
printf("arg=%d\n",*((int*)arg));
msg_id=msgget((key_t)101,0666|IPC_CREAT);
if(msg_id==-1)
{
printf("msgqueue not created\n");
}


data1.a=500;
data1.b=3;
data1.oper='*';
data1.tag=134;

fd=open("myfifo",O_WRONLY);
ret=write(fd,&data1,16);
printf("wrote %d no. of bytes in client7 and thread3\n",ret);

x=ptr2.tag;
msgrcv(msg_id,(void*)&ptr2,8,x,0);
printf("RESULT IN requesting client7 and thread3 and  tag = %d and %d\n",ptr2.res,ptr2.tag);
printf("---------------------------END OF CLIENT7  thread3----------------------------------\n");
close(fd);
pthread_exit(&arg2);
}





void *thread4(void *arg)
{
int arg2=1,msg_id;
long int x;
printf("arg=%d\n",*((int*)arg));
msg_id=msgget((key_t)101,0666|IPC_CREAT);
if(msg_id==-1)
{
printf("msgqueue not created\n");
}


data1.a=1000;
data1.b=100;
data1.oper='/';
data1.tag=135;

fd=open("myfifo",O_WRONLY);
ret=write(fd,&data1,16);
printf("wrote %d no. of bytes in client7 and thread4\n",ret);

x=ptr3.tag;
msgrcv(msg_id,(void*)&ptr3,8,x,0);
printf("RESULT IN requesting client7 and thread4 and  tag = %d and %d\n",ptr3.res,ptr3.tag);
printf("---------------------------END OF CLIENT7  thread4----------------------------------\n");
close(fd);
pthread_exit(&arg2);
}
