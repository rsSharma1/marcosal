#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
struct data{
int a,b;
int tag;
char oper;
};
struct result{
int res;
int tag;
};
int main(int argc,char *argv[])
{
int fd,ret,msg_id;
struct data data1;
struct result ptr;
data1.a=10;
data1.b=20;
data1.oper='+';
data1.tag=111;
fd=open("myfifo",O_WRONLY);
ret=write(fd,&data1,16);
printf("wrote %d no. of bytes\n",ret);
/*****************************************************************/
msg_id=msgget((key_t)101,0666|IPC_CREAT);
if(msg_id==-1)
{
printf("msgqueue not created\n");
}
msgrcv(msg_id,(void*)&ptr,8,0,0);
printf("RESULT IN requesting client and tag = %d and %d\n",ptr.res,ptr.tag);
return 1;
}
