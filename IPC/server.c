#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<sys/shm.h>
#include<sys/sem.h>
#include<pthread.h>

void *thread1(void*);
void *thread2(void*);
void *thread3(void*);
void *thread4(void*);

union semun {
	int val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
	struct seminfo  *__buf; 
}sem;

struct datarec{
	int a1,b1;
	int tag;
	char oper;
};

struct array{
	int arr[3];
	long int msg_type;
	char str[10];
};

struct result{
	int res;
	int tag;
};

int fd1[2];
char buf1[4],buf2[4];
struct result ptr,ptr1;
struct array *master_array;
int f,msg_idk;

int main()
{
	int ret,k,shm_id,msg_id,i=0,key=0,arg,arg1;
	pthread_t thd_id[4];

	struct datarec datarec,datarec1;
	struct array *str;
	/************************************************************/
	//access fifo
	if(access("myfifo",F_OK)==-1)
	{
		ret=mkfifo("myfifo",0777);
		if(!ret)
		{
			perror("myfifo");
			goto OUT;
		}
	}
	/************************************************************************/
	/********************SHM*************************************/
	shm_id=shmget((key_t)421,12,0666 | IPC_CREAT);
	if(shm_id==-1)
	{
		printf("ERROR IN SHMGET");
	}
	printf("shmid in server =%d\n",shm_id);

	/*************************************************************************/
	//access pipes
	ret=pipe(fd1);
	if(ret==-1)
	{
		printf("pipe not formed\n");
	}

	/****************************************************************************/
	//mesage queue
	msg_id=msgget((key_t)101,0666|IPC_CREAT);
	if(msg_id==-1)
	{
		printf("ERROR IN MSGGET");
	}
	printf("msg_id=%d\n",msg_id);

	msg_idk=msg_id;
	/*****************************************************************************/
	key=semget(111,1,0666|IPC_CREAT);
	printf("SEMAPHORE key=%d\n",key);
	sem.val=1;
	ret=semctl(key,0,SETVAL,sem);
	if(ret==-1)
		printf("semctl not working\n");


	while(1)
	{
		f=open("myfifo",O_RDONLY);
		ret=read(f,&datarec,16);
		printf("read this %d no. of bytes and a= %d b=%d oper=%c tag=%d\n",ret,datarec.a1,datarec.b1,datarec.oper,datarec.tag);

		switch(datarec.oper)
		{
			case '+' :
				printf("master_array in server is=%p\n",master_array);

				master_array=shmat(shm_id,NULL,0);	
				
				strcpy(master_array->str,"SERVER");
				printf("string in server=%s\n",master_array->str);
				master_array->arr[0]=datarec.a1;
				master_array->arr[1]=datarec.b1;
				master_array->arr[2]=datarec.tag;
				master_array->msg_type=12;


				ret=pthread_create(&thd_id[0],NULL,thread1,&msg_idk);
				if(ret!=0)
					printf("thread add not created\n");

				//pthread_join(thd_id[0],(void*)&arg1);

				break;



			case '-' :
				printf("master_array in server is=%p\n",master_array);
				master_array=shmat(shm_id,NULL,0);	
				strcpy(master_array->str,"SERVER");
				printf("string in server=%s\n",master_array->str);
				master_array->arr[0]=datarec.a1;
				master_array->arr[1]=datarec.b1;
				master_array->arr[2]=datarec.tag;
				master_array->msg_type=12;



				ret=pthread_create(&thd_id[1],NULL,thread2,&msg_idk);
				if(ret!=0)
					printf("thread add not created\n");
				//pthread_join(thd_id[1],(void*)&arg1);


				break;



			case '*' :
				printf("master_array in server is=%p\n",master_array);
				master_array=shmat(shm_id,NULL,0);	
				strcpy(master_array->str,"SERVER");
				printf("string in server=%s\n",master_array->str);
				master_array->arr[0]=datarec.a1;
				master_array->arr[1]=datarec.b1;
				master_array->arr[2]=datarec.tag;
				master_array->msg_type=12;
				ret=pthread_create(&thd_id[3],NULL,thread3,&msg_idk);
				if(ret!=0)
					printf("thread add not created\n");
				break;

			case '/' :
				printf("master_array in server is=%p\n",master_array);
				master_array=shmat(shm_id,NULL,0);	
				strcpy(master_array->str,"SERVER");
				printf("string in server=%s\n",master_array->str);
				master_array->arr[0]=datarec.a1;
				master_array->arr[1]=datarec.b1;
				master_array->arr[2]=datarec.tag;
				master_array->msg_type=12;

				ret=pthread_create(&thd_id[4],NULL,thread4,&msg_idk);
				if(ret!=0)
					printf("thread add not created\n");
				break;

			default:
				printf("operation not in the list\n");
				break;
		}
		datarec.tag=0;
		datarec.oper='\0';
	}
	//	shmctl(shm_id,IPC_RMID,0);
	//	msgctl(msg_id,IPC_RMID,0);
	return 0;
OUT:
	return -1;
}






void * thread1(void* msg_id1)
{
	int *msg_id,ret,k,retval;
	msg_id=(int*)msg_id1;


	ret=fork();
	if(ret==0)
	{
		sprintf(buf1,"%d",fd1[0]);
		sprintf(buf2,"%d",fd1[1]);
		execl("./processadd","processadd",buf1,buf2,NULL);
		printf("execl failed");
	}
	else
	{
		ret=wait(&k);
		printf("processadd pid is %d\n",ret);



		ret=read(fd1[0],&ptr,8);
		printf("no. of bytes read from processadd=%d\n",ret);
		printf("res=%d\n",ptr.res);
		printf("tag=%d\n",ptr.tag);
		//printf("msg_id in server for + is %d\n",msg_id);
		
		msgsnd(msg_idk,(void*)&ptr,8,0);
		printf("---------------------END OF ADDITION---------------------\n");
		shmdt(master_array);
		close(f);}
	//pthread_exit(&retval);
}


void * thread2(void* msg_id1)
{
	int *msg_id,ret,k,retval;
	msg_id=(int*)msg_id1;

	ret=fork();
	if(ret==0)
	{
		sprintf(buf1,"%d",fd1[0]);
		sprintf(buf2,"%d",fd1[1]);
		execl("./processsub","processsub",buf1,buf2,NULL);
		printf("execl failed");
	}
	else
	{
		ret=wait(&k);
		printf("processsub pid is %d\n",ret);



		ret=read(fd1[0],&ptr1,8);
		printf("no. of bytes read from processsub=%d\n",ret);
		printf("res=%d\n",ptr1.res);
		printf("tag=%d\n",ptr1.tag);
		
		msgsnd(msg_idk,(void*)&ptr1,8,0);
		
		printf("---------------------END OF SUBTRACTION---------------------\n");
		shmdt(master_array);
		close(f);}
	//pthread_exit(&retval);
}



void * thread4(void* msg_id1)
{
	int *msg_id,ret,k,retval;
	msg_id=(int*)msg_id1;




	ret=fork();
	if(ret==0)
	{
		sprintf(buf1,"%d",fd1[0]);
		sprintf(buf2,"%d",fd1[1]);
		execl("./processdiv","processdiv",buf1,buf2,NULL);
		printf("execl failed");
	}
	else
	{
		ret=wait(&k);
		printf("processdiv pid is %d\n",ret);


	}
	ret=read(fd1[0],&ptr,8);
	printf("no. of bytes read from processdiv=%d\n",ret);
	printf("res=%d\n",ptr.res);
	printf("tag=%d\n",ptr.tag);
	msgsnd(msg_idk,(void*)&ptr,8,0);
	printf("---------------------END OF DIVIDE---------------------\n");
	shmdt(master_array);
	close(f);


}




void * thread3(void* msg_id1)
{
	int *msg_id,ret,k,retval;
	msg_id=(int*)msg_id1;

	ret=fork();
	if(ret==0)
	{
		sprintf(buf1,"%d",fd1[0]);
		sprintf(buf2,"%d",fd1[1]);
		execl("./processmul","processmul",buf1,buf2,NULL);
		printf("execl failed");
	}
	else
	{
		ret=wait(&k);
		printf("processmul pid is %d\n",ret);


	}
	ret=read(fd1[0],&ptr,8);
	printf("no. of bytes read from processmul=%d\n",ret);
	printf("res=%d\n",ptr.res);
	printf("tag=%d\n",ptr.tag);
	msgsnd(msg_idk,(void*)&ptr,8,0);
	printf("---------------------END OF MULTIPLY---------------------\n");
	shmdt(master_array);
	close(f);


}
