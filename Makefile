INSTALL_DIR=modules

ifneq (${KERNELRELEASE},)
	obj-m := rahul.o

rahul-objs :=sopen.o write.o read2.o init.o ioctl.o scullseek.o close.o exitt.o

else
	KERNELDIR ?= /lib/modules/$(shell uname -r)/build 
	PWD := $(shell pwd)
default:
	@echo ${CFLAGS}
	@echo ${MAKE}
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules -Wno-error
	@echo ${CFLAGS}
	@rm -rf ${INSTALL_DIR}
	@mkdir ${INSTALL_DIR}
	@mv -f *.o *.ko *.mod.c .*.cmd ${INSTALL_DIR}
clean:
	rm -rf ${INSTALL_DIR}
endif
