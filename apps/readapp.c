#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include"../ioctlapp.h"
#include<sys/ioctl.h>

int main()
{
	int fd;
	unsigned long ret,arg;
	int val=0;
        char string[100];

	fd=open("node0",O_RDWR);
	printf("File descriptor(R/W) : %d\n",fd);
	
	lseek(fd,0,SEEK_SET);
	ret = read(fd, string, 23);
	printf("String Read :  %s\n",string);
	printf("Read %d characters\n",ret);
}
