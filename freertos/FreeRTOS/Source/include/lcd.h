#ifndef __LCD_INIT
#define __LCD_INIT

#include <lpc213x.h>
#include <stdint.h>
#include "freeRTOS.h"
#include "task.h"
#include "queue.h"


typedef struct message{
char *message;	
	
}message_t;

#define LINE_1 0x80
#define LINE_2 0xC0

void lcd_init(void);
QueueHandle_t mStartLcdTask();
void send_data_to_lcd_line(unsigned char p[], uint8_t LINENO);


#endif