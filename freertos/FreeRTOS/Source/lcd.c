#include "freeRTOS.h"
#include "task.h"
#include "lcd.h"


#define EN_high() IO0SET=(1<<18)			 
#define	EN_low()  IO0CLR=(1<<18)			 
#define lcd_small_delay 2


QueueHandle_t mStartLcdTask();
QueueHandle_t queue1;



void cmd(unsigned char);

void data(unsigned char);

void Delay(unsigned char j);

void Busywait(void);

/*------------------------------------------------------------------------*/
void lcd_init() {
	PINSEL0 = 0x00000000;		// Enable GPIO on all pins
 	PINSEL1 = 0x00000000;
 	PINSEL2 = 0x00000000;
	portENTER_CRITICAL();
	
	vTaskDelay(lcd_small_delay);

	IO0DIR=(1<<16)|(1<<17)|(1<<18)|(1<<19)|(1<<20)|(1<<21)|(1<<22)|(1<<1)|(1<<2)|(1<<3)|(1<<4);

	cmd(0x02);	 		//Retrun Home
	vTaskDelay(lcd_small_delay);
	cmd(0x28);			// LCD in 4 bit mode
	vTaskDelay(lcd_small_delay);
	cmd(0x0C);	  
	vTaskDelay(lcd_small_delay);	
	cmd(0x06);	
  vTaskDelay(lcd_small_delay);	

	portEXIT_CRITICAL();
}


void send_data_to_lcd_line(unsigned char p[], uint8_t LINENO) {
	unsigned int counter=0;
	cmd(0x01);
	if(LINENO != '\0' && p != '\0') {
	   cmd(LINENO);
		 for(counter=0;p[counter]!='\0';counter++)
		{			 
			data(p[counter]);
		}
	
	}
}
 
void cmd(unsigned char data)
{
 	unsigned char temp;
	temp=data;
	portENTER_CRITICAL();
	IO0CLR=(1<<16)|(1<<17)|	(1<<18);
	IO0CLR=(1<<19)| (1<<20)|(1<<21)|  (1<<22);
	IO0SET = (temp & 0xF0) << 15;
	IO0SET = (1<<18);
	vTaskDelay(lcd_small_delay);
	IO0CLR = (1<<18);
	IOCLR0=(1<<19)| (1<<20)|(1<<21)|  (1<<22);
	temp |=(data&0x0F);
	IO0SET = (temp << 19); 
	EN_high();
	EN_low();
	Busywait();
	vTaskDelay(lcd_small_delay);
	portEXIT_CRITICAL();
}	
/*********************************************************************/
void data(unsigned char command)
{
	unsigned char temp;
	temp=command;
	portENTER_CRITICAL();
	
	IO0CLR=(1<<16)|(1<<17)|	(1<<18)|(1<<19)| (1<<20)|(1<<21)| (1<<22);
	IO0SET=(1<<16);
	IO0SET = (temp & 0xF0) << 15;
	IO0SET = (1<<18);
	vTaskDelay(lcd_small_delay);
	IO0CLR = (1<<18);
	IOCLR0=(1<<19)| (1<<20)|(1<<21)|  (1<<22);
	temp|=(command&0x0F);
	IO0SET=(1<<16);
	IO0SET = temp<< 19;
	EN_high();
	EN_low();
	portEXIT_CRITICAL();
}

/***************************************************************************/

/****************************************************************************/
void Busywait()
{
//	unsigned char n;
	EN_low();
	IO0CLR=(1<<16);		// RS=0
	IO0SET=(1<<17);		//RW=1
	//IO0PIN&=0xFF87FFFF;	
	IO0DIR&=0xFF87FFFF;	// Making D7 pin as input
 	IO0PIN|=0x00400000;

	do
	{
			EN_high();
			EN_low();
			EN_high();
			EN_low();
		//	n=IO0PIN;
		
	}  
	while((IO0PIN&0x00400000)==0x00400000);
	EN_low();
	IO0CLR=(1<<17);		//RW=0
	//IO0DIR&=0xFF80FFFF;
	IO0DIR|=0x007F0000;
  
}

void TASK_LCD(char *pvarg) {
	int count=0 ;
	message_t message_recv;
	lcd_init();
	send_data_to_lcd_line("FREERTOS-LPC2136",LINE_1);
	for(;;) {
		count++;
		while(xQueueReceive(queue1,&message_recv,portMAX_DELAY) != pdPASS);
		cmd(0x01);
		if(count%2 ==0)
		send_data_to_lcd_line(message_recv.message,LINE_1);
		else
		send_data_to_lcd_line(message_recv.message,LINE_2);
		vTaskDelay(lcd_small_delay);
	}
	
}



QueueHandle_t mStartLcdTask(void) {
	queue1 = xQueueCreate(3,sizeof(message_t));
	
	xTaskCreate((TaskFunction_t)TASK_LCD,"LCD",500,NULL,7,NULL);
	       
 return queue1;
		
}
/******************************************************************************/
