#include "freeRTOS.h"
#include "task.h"
#include <string.h>
#include "lcd.h"
#include "ri2c.h"
#include "queue.h"
#include "semphr.h"

message_t message_1, message_2, message_3;
QueueHandle_t Queue_LCD;
xSemaphoreHandle Binary_Sem;

//-------------------------------------------------------------------------------------------------------------
void blink_LED1(void) {
	
 for(;;) {
		IO0SET = ((unsigned long)1<<1);
		vTaskDelay(200);
		IO0CLR = ((unsigned long)1<<1);
		vTaskDelay(200);
	}
}

void blink_LED2(void) {
message_2.message="LED-2";
	for(;;) {
		IO0SET = ((unsigned long)1<<2);
		vTaskDelay(200);
		IO0CLR = ((unsigned long)1<<2);
		vTaskDelay(200);
	}
}

void blink_LED3(void) {
 for(;;) {
		IO0SET = ((unsigned long)1<<3);
		vTaskDelay(200);
		IO0CLR = ((unsigned long)1<<3);
		vTaskDelay(200);
	}
}
//--------------------------------------------------------------------------------------------------------


void queue_task1(void) {
	message_1.message = "Queue-task1" ;
	
	for(;;) {
		xQueueSend(Queue_LCD,&message_1.message,(TickType_t)0);
		vTaskDelay(400);
	}
}

void queue_task2(void) {
	message_2.message = "Queue-task2" ;
	
	for(;;) {
		xQueueSend(Queue_LCD,&message_2.message,(TickType_t)0);
		vTaskDelay(600);
	}
}


void queue_task3(void) {
	message_1.message = "Queue-task3" ;
	
	for(;;) {
		xSemaphoreTake(Binary_Sem,portMAX_DELAY);
		xQueueSend(Queue_LCD,&message_1.message,(TickType_t)0);
		vTaskDelay(600);
		xSemaphoreGive(Binary_Sem);
		vTaskDelay(6);
	}
}

void queue_task4(void) {
	message_2.message = "Queue-task4" ;
	
	for(;;) {
		xSemaphoreTake(Binary_Sem,portMAX_DELAY);
		xQueueSend(Queue_LCD,&message_2.message,(TickType_t)0);
		vTaskDelay(600);
		xSemaphoreGive(Binary_Sem);
		vTaskDelay(5);
	}
}


int main() {
	PINSEL0 = 0x00000000;
	PINSEL1 =0x00000000;
	
  IO0DIR = 0x0000000F;	

//Ist POC
//	xTaskCreate((TaskFunction_t)blink_LED1,"LED1",128,NULL,3,NULL);
//	xTaskCreate((TaskFunction_t)blink_LED2,"LED2",128,NULL,2,NULL);
//	xTaskCreate((TaskFunction_t)blink_LED3,"LED3",128,NULL,2,NULL);

//2nd POC
	Queue_LCD = mStartLcdTask();
	xTaskCreate((TaskFunction_t)queue_task1,"Q-task1",128,NULL,3,NULL);
	xTaskCreate((TaskFunction_t)queue_task2,"Q-task2",128,NULL,2,NULL);

//3rd POC
//vSemaphoreCreateBinary(Binary_Sem);
//	
//	Queue_LCD = mStartLcdTask();
//	xTaskCreate((TaskFunction_t)queue_task3,"Q-task3",128,NULL,2,NULL);
//	xTaskCreate((TaskFunction_t)queue_task4,"Q-task4",128,NULL,2,NULL);
	
	vTaskStartScheduler();
	for(;;);
	return 0;
}