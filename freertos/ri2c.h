#ifndef __RI2C__
#define __RI2C__


#define EEPROM_Addr 0xA0     //device address
#define I2Cwrite 0x00   //LSB bit 0 (write)
#define I2Cread  0x01   //LSB bit 1 (read)

#define I2C_ENABLE  1 << 6     //I2C Enable bit
#define I2C_START 1 << 5     //Start Bit
#define I2C_STOP  1 << 4     //Stop Bit
#define I2C_SI  1 << 3     //I2C interrupt flag
#define I2C_AACK   1 << 2     //assert ACK flag

void I2CInit(void);
void I2CStart(void);
void I2CStop(void);
void I2Csend(unsigned char data);
unsigned char I2Cget(void);

#endif