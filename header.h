#include<linux/module.h>
#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/version.h>
#include<linux/moduleparam.h>
#include<linux/fs.h>
#include<linux/slab.h>
#include<linux/cdev.h>
#include<linux/export.h>
#include<linux/uaccess.h>
#include<linux/ioctl.h>
#include<linux/jiffies.h>
#include<linux/timer.h>
#include<linux/completion.h>
#include<linux/spinlock.h>
#include<linux/wait.h>
#include"ioctlapp.h"

#define DEVICE_NAME "RAHUL"

#ifndef DEBUG
#define DEBUG 1
#endif


#ifndef Q_SIZE
#define Q_SIZE 8
#endif


#ifndef QSET_SIZE
#define QSET_SIZE 8
#endif


MODULE_LICENSE("GPL");
MODULE_AUTHOR("RAHUL");
MODULE_DESCRIPTION("initialize and exitting the function");



int scullopen(struct inode *,struct file *);
int scullrelease(struct inode *,struct file *);
ssize_t write_fuct(struct file *,const char __user *,size_t,loff_t *);
//ssize_t write_server(struct file *,const char __user *,size_t size,loff_t *);
//ssize_t read1_fuct(struct file *,char __user *,size_t,loff_t *);
ssize_t read2_fuct(struct file *,char __user *,size_t,loff_t *);
struct ScullQset *create_scull(int);
int allocate_qset(struct ScullQset *,int,int);
char *allocate_quantum(int);

//**********************lseek****************************************************
loff_t scullseek(struct file *,loff_t,int);

//**************************ioctl**************************************************
long scull_ioctl(struct file *, unsigned int, unsigned long);
//******************************************************************************

struct ScullQset{
	struct ScullQset *next;
	void **data;
};

struct ScullDev
{
	struct ScullQset *scullqset;
	int qset_size;
	int quantum_size;
	int device_size;
	int data_size;
	struct cdev c_dev;
};


struct jit
{
	struct timer_list tl;
	unsigned long prevjiffies;
	unsigned char *buff;
	int loops;
};

struct data{
	int a;
	int b;
	char oper;
};
