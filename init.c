#include"header.h"
#include"declaration.h"

struct file_operations fops={
     open:scullopen,
     release:scullrelease,
     write:write_fuct,
     read:read2_fuct,
     llseek:scullseek,
     unlocked_ioctl:scull_ioctl,
};

//Kernel-driver parameters
int nod,n,lv,quantum_size,qset_size,device_size;

module_param(nod,int,S_IRUGO);
module_param(n,int,S_IRUGO);
module_param(qset_size,int,S_IRUGO);
module_param(quantum_size,int,S_IRUGO);
module_param(device_size,int,S_IRUGO);



//Kernel Timers

struct jit jit_data,jit_data1;
void kernel_timer(unsigned long a)
{
#ifdef DEBUG
	printk(KERN_ALERT"IN KERNEL_TIMER()\n");
	printk(KERN_INFO"a=%ld\n",a);
#endif
}
/***************************************************************************************/
//int flager=1;
//struct completion my_completion;
rwlock_t my_lock;
wait_queue_head_t my_queue;
dev_t dev,newdev;
struct ScullDev *sculldev;


//Module Instializaion starts
static int modules_initialization(void)
{
	unsigned long m=0;
	int ret,majorno,minorno;

#ifdef DEBUG
	printk(KERN_INFO"Hello!!! I am coming\n");
	printk(KERN_INFO"nod value is %d\n",nod);
#endif

	ret = alloc_chrdev_region(&dev,0,n,DEVICE_NAME);
	if(ret==-1)
	{

#ifdef DEBUG
	printk(KERN_INFO"FAILURE!!! Bug in the program\n");
#endif

	}
	
	majorno = MAJOR(dev);
	minorno = MINOR(dev);

#ifdef DEBUG
	printk(KERN_INFO"MAJOR NO. = %d\n",majorno);
	printk(KERN_INFO"MINOR NO. = %d\n",minorno);
#endif

	sculldev = (struct ScullDev*)kmalloc(sizeof(struct ScullDev)*n,GFP_KERNEL);
	printk(KERN_ALERT"scull memory address is %p\n",sculldev);
	if(!sculldev)
	{

#ifdef DEBUG
		printk(KERN_ALERT"ERROR!! in memory allotaion");
#endif

		goto OUT;
	}

	memset(sculldev,'\0',sizeof(struct ScullDev)*n);

	for(lv=0;lv<n;lv++)
	{
		newdev = MKDEV(majorno,lv);

		cdev_init(&sculldev[lv].c_dev,&fops);

		sculldev[lv].c_dev.owner = THIS_MODULE;
		sculldev[lv].c_dev.ops = &fops;
		sculldev[lv].c_dev.dev = newdev;
		sculldev[lv].c_dev.count = n;
		sculldev[lv].qset_size = qset_size;
		sculldev[lv].quantum_size = quantum_size;
		sculldev[lv].device_size = device_size;

#ifdef DEBUG
		printk(KERN_INFO"cdev(%d) initialization\n",lv);
#endif

		minorno=MINOR(newdev);
		ret=cdev_add(&sculldev[lv].c_dev,newdev,1);
		if(ret<0)
		{	

#ifdef DEBUG
			printk(KERN_ERR "cdev_add fails!!!!!!!\n");
#endif

		}

#ifdef DEBUG
		printk(KERN_INFO"cdev(%d) addition majornumber=%d and minornumber=%d\n",lv,majorno,minorno);
#endif

		majorno =MAJOR(newdev);
		minorno=MINOR(newdev);

#ifdef DEBUG
		printk(KERN_INFO"%d MAJOR NUMBER=%d\n",lv,majorno);
		printk(KERN_INFO"%d MINOR NUMBER=%d\n",lv,minorno);
#endif


	}

	/***************************************************************************************************/
	return 0;
OUT:
	return -1;
}


module_init(modules_initialization);
