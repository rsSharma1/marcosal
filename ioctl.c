#include"header.h"

long scull_ioctl(struct file *filep,unsigned int command,unsigned long arg)
{
	struct ScullDev *lsculldev;

	lsculldev=filep->private_data;
	int val=0;

	if(!lsculldev)
		goto OUT;

#ifdef DEBUG
	printk(KERN_INFO"BEGIN :%s\n",__func__);
	printk(KERN_INFO"COMMAND:%x\n",command);
#endif
	switch(command)
	{
		case SCULL_IOCSQUANTUM :
			get_user(lsculldev->quantum_size,&arg);
			printk(KERN_INFO"new quantum_size=%d\n",lsculldev->quantum_size);
			val=lsculldev->quantum_size;
			break;

		case SCULL_IOCGQUANTUM :
			val=lsculldev->quantum_size;
			break;

		case SCULL_IOCGQSET :
			val = lsculldev->qset_size;
			break;

		case SCULL_IOCSQSET :
			get_user(lsculldev->qset_size,&arg);
			printk(KERN_INFO"new qset_size=%d\n",lsculldev->qset_size);
			val=lsculldev->qset_size;
			break;

		default:
			break;
	}

#ifdef DEBUG
	printk(KERN_INFO"END :%s",__func__);
#endif
	return val;
OUT:
	val=-1;

#ifdef DEBUG
	printk(KERN_INFO"ERROR IN SCULLDEV");
#endif

	return val;
}

