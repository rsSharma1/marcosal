#include"header.h"
#include"declaration.h"

ssize_t read2_fuct(struct file *filep,char __user *buffer,size_t size,loff_t *off)
{
	int ret,noq,lsize,lnoq,lv,nocsr=0,noctr=0,quantum=0,qset=0,offset=0,qpos,spos,bpos,data;
	struct ScullDev *lsculldev;
	struct ScullQset *lscullqset;
	unsigned long j;

#ifdef DEBUG
	printk(KERN_INFO"BEGIN :%s\n",__func__);
#endif
	j=jiffies;

#ifdef DEBUG
	printk(KERN_INFO"jiffies values at read startup=%ld\n",j);
#endif

#ifdef DEBUG
	printk(KERN_INFO"offset :%d size=%d offset=%d\n",(int)filep->f_pos,(int)size,(int)*off);
#endif
	//flager=1;
	//printk(KERN_INFO "going to wait");
	//wait_event(my_queue,flager==0);
	lsculldev = filep->private_data;
	lscullqset = lsculldev->scullqset;
	quantum = lsculldev->quantum_size;
	qset = lsculldev->qset_size;
	data = lsculldev->data_size;

#ifdef DEBUG
	printk(KERN_INFO"quantum :%d qset=%d data=%d lsculldev=%p lscullqset=%p\n",quantum,qset,data,lsculldev,lscullqset);
#endif

	offset=(int)*off;
	lsize=(int)size;
	if((offset+lsize)>=data)
	{
		goto OUT;
	}
	noctr=lsize;
	noq=lsize/quantum;
	if(lsize%quantum!=0)
		noq++;
	spos=offset/(qset*quantum);
	for(lv=spos;lv>0;lv--)
	{
		lscullqset=lscullqset->next;
	}
	qpos=(offset%(qset*quantum))/quantum;

	bpos=offset%quantum;



	printk(KERN_INFO"qpos=%d bpos=%d spos=%d noq=%d lsize=%d lscullqset=%p\n",qpos,bpos,spos,noq,lsize,lscullqset);
	/***********************************main code****************************************************/
	//wait_for_completion(&my_completion);
	read_lock(&my_lock);
	while(lscullqset)
	{
		if(noq>quantum)
			lnoq=quantum;
		else
		{
			lnoq=noq+qpos;
			//	if(qpos!=0)//addition logic
			//	{lnoq=lnoq+qpos;}
		}
		for(lv=qpos;lv<lnoq;lv++)
		{
			if(noctr>quantum)
				noctr=quantum;
			ret = copy_to_user(buffer+nocsr,lscullqset->data[lv]+bpos,noctr);
			noq--;
			nocsr=nocsr+(noctr-ret);
			noctr=lsize-nocsr;
			printk(KERN_INFO"NUMBER of characters read in %d iteration is %d\n",lv,nocsr);
			bpos=0;
			if(noctr==0)
				goto loop;
		}
		qpos=0;
		lscullqset=lscullqset->next;
	}
	//complete(&my_completion);
loop:

	*off+=nocsr;

#ifdef DEBUG
	printk(KERN_ALERT"END :%s\n",__func__);
#endif

	read_unlock(&my_lock);
	j=jiffies;

#ifdef DEBUG
	printk(KERN_INFO"jiffies values at read endup=%ld\n",j);
#endif

	return nocsr;
OUT:

#ifdef DEBUG
	printk(KERN_ALERT"ERROR IN MEMORY OR LSEEK greater than data");
#endif
	return 0;
}
