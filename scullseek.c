#include"header.h"


loff_t scullseek(struct file *filep,loff_t offset,int origin)
{
	struct ScullDev *lsculldev;
	loff_t newpos;

#ifdef DEBUG
	printk(KERN_INFO"BEGIN : %s\n",__func__);
#endif
	lsculldev = filep->private_data;

#ifdef DEBUG
	printk(KERN_INFO"DATA SIZE IN SCULLDEV=%d\n",(int)lsculldev->data_size);
#endif
	if(!lsculldev)
	{

#ifdef DEBUG
		printk(KERN_ERR"NO SCULLDEV AVAILABLE");
#endif

	}

	switch(origin)
	{
		case 0 :
			newpos = offset;
			break;
		case 1 :
			newpos = filep->f_pos + offset;
			break;
		case 2 :
			newpos = lsculldev->data_size - offset;
			break;

		default :
			newpos = -1;
			break;
	}


	if(newpos == -1)
	{

#ifdef DEBUG
		printk(KERN_ERR"newpos error not valid !!!!!");
#endif

	}


	filep->f_pos = newpos;

#ifdef DEBUG
	printk(KERN_INFO"END : %s\n",__func__);
#endif
	return newpos;
}
