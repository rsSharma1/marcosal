#include"header.h"

int scullopen(struct inode *inodep,struct file *filep)
{
	struct ScullDev *lsculldev;
	
	lsculldev = container_of(inodep->i_cdev,struct ScullDev,c_dev);
	
	filep->private_data = lsculldev;
	
	if((filep->f_flags & O_ACCMODE) == O_WRONLY)
	{
#ifdef DEBUG
		printk(KERN_INFO"write permission given\n");
#endif
	}

	if((filep->f_flags & O_ACCMODE) == O_RDONLY)
	{
#ifdef DEBUG
		printk(KERN_INFO"read permission given\n");
#endif

	}
	if((filep->f_flags & O_ACCMODE) == O_RDWR)
	{
#ifdef DEBUG
		printk(KERN_INFO"read/write permission given\n");
#endif
	}


#ifdef DEBUG
	printk(KERN_INFO"BEGIN : %s\n",__func__);
#endif


#ifdef DEBUG
	printk(KERN_INFO"END : %s\n",__func__);
#endif

	return 0;
}
