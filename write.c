#include"header.h"
#include"declaration.h"

ssize_t write_fuct(struct file *filep,const char __user *buffer,size_t size,loff_t *llseek)
{
	struct ScullDev *lsculldev;
	struct ScullQset *lscullqset;
	void *p;
	int rtt;
	int lqset,lquantum,lsize,noqset,noquantum,ret=0,lv=0,nocsw=0,noctw=0,qset,quantum;
	unsigned long j,k,timeout;

#ifdef DEBUG
	printk(KERN_INFO"jiffies value at write startup=%ld\n",j);
#endif

	lsize = (int)size;
	lsculldev = filep->private_data;
	qset = (int)lsculldev->qset_size;
	quantum = (int)lsculldev->quantum_size;
	noctw = lquantum=quantum;
	lqset = qset;

#ifdef DEBUG
	printk(KERN_INFO"lseek BEGIN value =%d\n",(int)*llseek);
#endif

#ifdef DEBUG
	printk(KERN_INFO"lseek value struct file =%d\n",(int)filep->f_pos);
#endif

#ifdef DEBUG
	printk(KERN_INFO"lsculldev->qset_size=%d\n",(int)lsculldev->qset_size);
#endif

#ifdef DEBUG
	printk(KERN_INFO"lsculldev->quantum_size=%d\n",(int)lsculldev->quantum_size);
#endif

	noqset = lsize/(qset*quantum);
	if((lsize%(qset*quantum) != 0))
	    noqset++;
#ifdef DEBUG
	printk(KERN_INFO"noqset=%d\n",noqset);
#endif
	noquantum = lsize/quantum;
	if((lsize%quantum) != 0)
		noquantum++;
#ifdef DEBUG
	printk(KERN_INFO"noquantum=%d\n",noquantum);
#endif

	lscullqset = create_scull(noqset);
	if(!lsculldev)
	{
#ifdef DEBUG
		printk(KERN_ERR"NO SUCH MEMORY\n");
#endif
	}

#ifdef DEBUG
	printk(KERN_ALERT"buffer =%s\n",buffer);
#endif
	lsculldev->scullqset = lscullqset;//linking of scullqset in lsculldev with lscullqset
	ret = allocate_qset(lscullqset,noqset,qset);
	if(ret==-1)
	{
		goto OUT;
	}
#ifdef DEBUG
	printk(KERN_INFO"size :%d lscullqset :%p noqset:%d noquantum :%d \n",lsize,lscullqset,noqset,noquantum);
#endif
	//write_lock(&my_lock);

	while(lscullqset)
	{
#ifdef DEBUG
		printk(KERN_INFO"QSET[%d]:\n",noqset);
#endif
		for(lv=0;lv<qset;lv++)
		{	
			p = allocate_quantum(quantum);
			if(!p)
			{
				goto OUT;
			}

			if(noctw>quantum)
				noctw=quantum;

			lscullqset->data[lv]=(void*)p;

			ret = copy_from_user(lscullqset->data[lv],buffer+nocsw,noctw);
#ifdef DEBUG
			printk(KERN_INFO"nocsw : %d and noctw :%d ret=%d\n",nocsw,noctw,ret);
#endif
			nocsw=nocsw+(noctw-ret);
			noctw=lsize-nocsw;
#ifdef DEBUG
			printk(KERN_INFO"QUANTUM[%d]:\n",lv);
#endif
#ifdef DEBUG
			printk(KERN_INFO"String wrote at specific location is %s",(char*)lscullqset->data[lv]);
#endif
			if (noctw==0)
			{goto loop;}

		}
		lscullqset = lscullqset->next;
	}
loop:	

#ifdef DEBUG
	*llseek+=nocsw;
	printk(KERN_INFO"lseek END value =%d\n",(int)*llseek);
#endif
	lsculldev->data_size=nocsw;
#ifdef DEBUG
	printk(KERN_INFO"file poinsition f_pos at end =%d\n",(int)filep->f_pos);
	printk(KERN_INFO"*llseek =%d\n",(int)*llseek);
#endif

	return nocsw;

OUT:
#ifdef DEBUG
	printk(KERN_ERR"ERROR");
#endif
	return -1;

}



char *allocate_quantum(quantum)
{
	char *arr;
#ifdef DEBUG
	printk(KERN_ALERT"BEGIN =%s\n",__func__);
#endif
	arr = (char*)kmalloc(sizeof(char)*quantum,GFP_KERNEL);
	memset(arr,'\0',sizeof(char)*quantum);
	if(!arr)
	{
		goto OUT;
	}
#ifdef DEBUG
	printk(KERN_ALERT"END %s\n",__func__);
#endif
	return arr;
OUT:
#ifdef DEBUG
	printk(KERN_ERR"ERROR");
#endif
	return arr;
}

int allocate_qset(struct ScullQset *lscullqset,int noqsets,int qset)
{
	int lv=0;
#ifdef DEBUG
	printk(KERN_ALERT"BEGIN =%s\n",__func__);
#endif
#ifdef DEBUG
	printk(KERN_INFO"noqsets =%d\n",noqsets);
#endif

	for(lv=0;lv<noqsets;lv++)
	{
		lscullqset->data=(void*)kmalloc(sizeof(void*) * qset,GFP_KERNEL);
		memset(lscullqset->data,'\0',sizeof(void*) * qset);
		if(!lscullqset->data)
		{
			goto OUT;
		}
		lscullqset = lscullqset->next;
	}
#ifdef DEBUG
	printk(KERN_ALERT"END =%s\n",__func__);
#endif
	return 0;
OUT:
#ifdef DEBUG
	printk(KERN_ERR"ERROR in memory allocation\n");
#endif
	return -1;
}



struct ScullQset* create_scull(int noqsets)
{
	struct ScullQset *lscullqset,*new,*temp;
	int lv,flag=1;
	lscullqset=NULL;
#ifdef DEBUG
	printk(KERN_INFO"BEGIN : %s\n",__func__);
#endif
#ifdef DEBUG
	printk(KERN_INFO"NO. OF QSET : %d\n",noqsets);
#endif


	for(lv=0;lv<noqsets;lv++)
	{
		new=(struct ScullQset *)kmalloc(sizeof(struct ScullQset),GFP_KERNEL);
		if(!new)
		{
			goto OUT;
		}
		new->next=NULL;
		new->data=NULL;
		if(flag==1)
		{
			lscullqset=temp=new;
			flag=0;
		}
		else
		{
			temp->next=new;
			temp=temp->next;

		}	

	}

#ifdef DEBUG
	printk(KERN_INFO"END : %s\n",__func__);
#endif
	return lscullqset;

OUT:
#ifdef DEBUG
	printk(KERN_ERR"ERROR no kernel memory left\n");
#endif
	return NULL;
}

